/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import Model.ConexionBDA;//

/**
 *
 * @author fido
 */
public class Producto {

    private int id;
    private String nombre;
    private double temperatura;
    private double valorBase;
    public double costo;
   
    
    public Producto() {
    }

    public Producto(int id, String nombre, double temperatura, double valorBase, double costo) {
        this.id = id;
        this.nombre = nombre;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
        this.costo = costo;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }
    public double calcularCosto(){
      if (temperatura > 21) {
            this.costo = Math.round(valorBase * 1.1);
            return costo;
        } else {
            this.costo = Math.round(valorBase * 1.2);
            return costo;
        }

    }
    
    
    @Override
    public String toString() {
        return "Producto{" + "id=" + id + ", nombre=" + nombre + ", temperatura=" + temperatura + ", valorBase=" + valorBase + ", costo=" + costo + '}';
    }
 
   

    

    public boolean guardarProducto() {
        ConexionBDA conexion = new ConexionBDA();
        String sql = "INSERT INTO Productos(nombre,Temperatura,ValorBase,Costo)"
                + "VALUES('" + this.nombre + "'," + this.temperatura + "," + this.valorBase + ","  + this.calcularCosto()+ ");";
        if (conexion.setAutoCommitBD(false)) {//Para que la bd no confirme automaticamente el cambio
            if (conexion.insertarBD(sql)) {
                conexion.commitBD();//confirma el cambio a la BD
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean actualizarProducto() {
        ConexionBDA conexion = new ConexionBDA();
        String sql = "UPDATE Productos SET nombre='"
                + this.nombre + "',Temperatura=" + this.temperatura
                + ",ValorBase=" + this.valorBase +",Costo=" +this.calcularCosto() + " WHERE id=" + this.id + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    
    public List<Producto> listaProductos() {
        List<Producto> listaProductos = new ArrayList<>();
        ConexionBDA conexion = new ConexionBDA();
        String sql = "SELECT * FROM Productos;";
        try {
            ResultSet rs = conexion.consultarBD(sql);
            Producto p;
            while (rs.next()) {
                p = new Producto();
                p.setId(rs.getInt("id"));
                p.setNombre(rs.getString("nombre"));
                p.setTemperatura(rs.getDouble("Temperatura"));
                p.setValorBase(rs.getDouble("ValorBase"));
                p.setCosto(rs.getDouble("Costo"));
                listaProductos.add(p);
                
            }
        } catch (SQLException ex) {
            System.out.println("Error al listar productos:" + ex.getMessage());
        } finally {
            conexion.cerrarConexion();
        }
        return listaProductos;
    }

    public boolean eliminarProducto() {
        ConexionBDA conexion = new ConexionBDA();
        String sql = "DELETE FROM productos WHERE id=" + this.id + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }



}
